Requires wurlitzer 1.0.1+, which can be installed by running this

```
pip install wurlitzer
```

If you don't have wurlitzer installed and on the PYTHONPATH, a local copy will
be imported and used, instead.
