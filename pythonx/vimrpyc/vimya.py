#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Send a file to Maya, using rpyc.

For more information on what rpyc is, look at this documentation:

https://media.readthedocs.io/pdf/rpyc/latest/rpyc.pdf
sidefx.com/docs/houdini/hom/rpc.html

'''

# IMPORT STANDARD LIBRARIES
import re

# IMPORT LOCAL LIBRARIES
# TODO : Change this import to be relative
from vimrpyc.core import base


class MayaRpycReplacer(base.RpycReplacer):

    '''A Maya-style class that's used to set up our Maya-rypc tool.

    Attributes:
        connection_template_line (str):
            The line that will be used to replace our Maya module(s).
        module_imports (tuple[<_sre.SRE_Pattern>]):
            The modules to replace with connection_template_line.
        rpyc_import (<_sre.SRE_Pattern>):
            The pattern that is used to find if rpyc is imported.
        rpyc_import_line (str):
            The line that is used to import rpyc.

    '''

    connection_template_line = 'connection, {name} = mrpyc.import_remote_module()'
    module_imports = (
        re.compile('from\s+pymel\s+import\s+core\s+as\s+(?P<namespace>\w+)'),
        re.compile('from\s+pymel\s+import\s+(?P<namespace>core)'),
        re.compile('import\s+pymel.core\s+as\s+(?P<namespace>\w+)'),
        re.compile('import\s+(?P<namespace>pymel.core)'),
    )
    rpyc_import = re.compile('from\s+vimrpyc\.servers\s+import\s+mrpyc')
    rpyc_import_line = 'from vimrpyc.servers import mrpyc'


class Vimya(base.VimCommands):

    '''The class needed to send/receive Maya python files to rpyc, in Vim.'''

    replacer = MayaRpycReplacer
