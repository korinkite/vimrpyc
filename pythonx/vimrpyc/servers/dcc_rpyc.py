'''This module provides access to Maya (via the pymel/maya.cmds module).

from a separate Python process. It uses the RPyC library (rpyc.wikidot.com).

To start a server (in a new thread), run:
    dcc_rpyc.start_server()
This function returns the thread that was created.

To attach to the server from a client, run:
    connection, mod = rpyc.import_remote_module()
The connection will remain open for the lifetime of the connection object,
and you can treat the imported module just as though you imported it.

'''

# IMPORT STANDARD LIBRARIES
import threading
import socket

# IMPORT LOCAL LIBRARIES
from ..vendor.rpyc.servers import classic_server
from ..vendor.rpyc import classic


DEFAULT_PORT = 12345


def start_server(port=DEFAULT_PORT, use_thread=True, quiet=True):
    '''Start the server.

    Note:
        quiet=False only applies when use_thread=False.

    Args:
        port (:obj:`int`, optional):
            The location on the server to listen to for commands.
            Default: 12345.
        use_thread (:obj:`bool`, optional):
            If True, the server will be created in a separate thread than
            the main thread. It's recommended that this is always True.
            Default is True.
        quiet (:obj:`bool`, optional):
            Set the server's logger to quiet. Default is True.

    '''
    if use_thread:
        thread = threading.Thread(
            target=lambda: start_server(port, use_thread=False))
        thread.start()
        return thread

    args = []
    if quiet:
        args.append('-q')
    args.extend(('-p', str(port), '--dont-register'))

    options, args = classic_server.parser.parse_args(args)
    options.registrar = None
    options.authenticator = None
    try:
        classic_server.serve_threaded(options)
    except socket.error as error:
        if error.errno == 48:  # Address is already in-use
            raise SystemExit('Restarting the server')


def import_remote_module(name, server='127.0.0.1', port=DEFAULT_PORT):
    '''Import the given remote server module.

    Note:
        This function will raise an exception "Connection Refused" if
        :func:`vimrpyc.servers.dcc_rpyc.start_server` is not run, first.

    Args:
        name (str, optional):
            The module to get from the remote server
        server (:obj:`str`, optional):
            The server to look for open connections.
            Default: '127.0.0.1', the localhost.
        port (:obj:`int`, optional):
            The location on the server to listen to for commands.
            Default: 12345.

    '''
    connection = classic.connect(server, port)
    return connection, connection.modules[name]
