#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''The Nuke version of rpyc.

In the interpreter that you want to act as your server (i.e. The Python shell
in Nuke), run this:

>>> from vimrpyc.servers import hrpyc
>>> hrpyc.start_server()

Now, in another Python interpreter, run this

>>> from vimrpyc.servers import hrpyc
>>> connection, pm = import_remote_module()

From there, you can interface with the :mod:`nuke` module exactly as you would
if it was in Nuke.

'''

# IMPORT LOCAL LIBRARIES
from . import dcc_rpyc


DEFAULT_PORT = 10005


def start_server(port=DEFAULT_PORT, use_thread=True, quiet=True):
    '''Start the server.

    Note:
        quiet=False only applies when use_thread=False.

    Args:
        port (:obj:`int`, optional):
            The location on the server to listen to for commands.
            Default: 10005.
        use_thread (:obj:`bool`, optional):
            If True, the server will be created in a separate thread than
            the main thread. It's recommended that this is always True.
            Default is True.
        quiet (:obj:`bool`, optional):
            Set the server's logger to quiet. Default is True.

    '''
    return dcc_rpyc.start_server(port=port, use_thread=use_thread, quiet=quiet)


def import_remote_module(name='nuke', server='127.0.0.1', port=DEFAULT_PORT):
    '''Import the given remote server module.

    Note:
        This function will raise an exception "Connection Refused" if
        :func:`vimrpyc.servers.nrpyc.start_server` is not run, first.

    Args:
        name (:obj:`str`, optional):
            The module to get from the remote server. Default: "nuke".
        server (:obj:`str`, optional):
            The server to look for open connections.
            Default: '127.0.0.1', the localhost.
        port (:obj:`int`, optional):
            The location on the server to listen to for commands.
            Default: 10005.

    '''
    return dcc_rpyc.import_remote_module(name=name, server=server, port=port)
