This module, rpyc, is a copy of rpyc-3.0.7. rpyc's latest version, as of writing 
(2018-02-02), is 3.4.4. But I found implementation for the older version of rpyc
so I just decided to add it directly. It also makes installation easier.

Feel free to upgrade to a later version of rpyc if you need it. But otherwise,
do not source this folder in PYTHONPATH or modify the rpyc folder unless it's
to upgrade the version.

wurlitzer is a copy of 1.0.1 and it is only used if wurlitzer isn't installed or on the PYTHONPATH
