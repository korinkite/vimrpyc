#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''The module that formats files and sends them with rpyc.'''

# IMPORT STANDARD LIBRARIES
import functools
import tempfile
import logging
import abc
import imp
import os
import re

# IMPORT THIRD-PARTY LIBRARIES
import vim

# IMPORT LOCAL LIBRARIES
from . import display


_VIM_CURRENT_FILE_NAMES = frozenset(('%', '%:p'))
_FUTURE_IMPORT = re.compile('^from\s+__future__\s+import\s+\w+(#.+)?$')
LOGGER = logging.getLogger()


class RpycReplacer(object):

    '''A class that's used to set up files to be sent to rpyc.

    Attributes:
        connection_template_line (str):
            The line that will be used to replace our the DCC module(s).
        module_imports (tuple[<_sre.SRE_Pattern>]):
            The modules to replace with connection_template_line.
        rpyc_import (<_sre.SRE_Pattern>):
            The pattern that is used to find if rpyc is imported.
        rpyc_import_line (str):
            The line that is used to import rpyc.

    '''

    module_imports = tuple()
    rpyc_import = re.compile(r'import\s+rpyc')
    rpyc_import_line = 'import rpyc'
    connection_template_line = 'connection, {name} = rpyc.import_remote_module()'

    def __init__(self):
        '''Initialize the object and do nothing else.'''
        super(RpycReplacer, self).__init__()

    @classmethod
    def is_rpyc_imported(cls, lines):
        '''bool: If the given Python import lines contains an import for hrpyc.'''
        lines = [line.strip() for line in lines]

        for line in lines:
            if cls.rpyc_import.match(line):
                return True

        return False

    @classmethod
    def get_module_import_name(cls, text):
        '''bool: If the given import line is importing a tracked-module.'''
        for option in cls.module_imports:
            try:
                return option.match(text).group('namespace')
            except AttributeError:
                pass

        return ''

    @classmethod
    def reformat_source_lines(cls, lines):
        '''Remove tracked modules and replace it with an rpyc connection.

        Args:
            lines (list[str]): The lines to replace, presumably read from a file.

        Returns:
            list[str]: The adjusted lines.

        '''
        lines = list(lines)  # Make a copy

        insert_index = _find_insert_index(lines) + 1
        lines.insert(insert_index, cls.rpyc_import_line)

        for index, line in enumerate(lines):
            name = cls.get_module_import_name(line)

            if not name:
                continue

            lines[index] = ''
            lines.insert(insert_index + 1, cls.connection_template_line.format(name=name))

        return lines

    @classmethod
    def get_default_imports(cls):
        '''list[str]: The default import statements needed for this class.'''
        return [
            cls.connection_template_line.format(name='hou')
        ]


class VimCommands(object):

    '''The primary class that's used to make files compatible with rpyc and Vim.

    Attributes:
        replacer (:class:`vimrpyc.core.base.RpycReplacer`):
            The class that is used to replace the lines in the file so that the
            file can be submitted to another application, using rpyc.
        displayer (:class:`vimrpyc.core.display`):
            The class that's needed to get back the response from a message
            that was sent to another application using rpyc and display it,
            in a text editor.

    '''

    replacer = RpycReplacer
    displayer = display.VimDisplayer

    @staticmethod
    @abc.abstractmethod
    def is_modified():
        '''bool: If the current buffer has unsaved changed.'''
        return not vim.eval('&modified')

    @staticmethod
    def get_all_lines():
        '''list[str]: Get every line in the Vim buffer (the current file).'''
        return list(vim.current.buffer)

    @staticmethod
    def get_current_file_path():
        '''str: The full path to the current file.'''
        return vim.eval("expand('%:p')")

    @classmethod
    def send_buffer(cls):
        '''Get a copy of the user's current buffer and send it to the server.

        If the current buffer is unmodified, just send the file, instead.
        That way, we have a record of the file that sent the source-code lines.

        Note:
            This method requires a server to be listening for lines, using rpyc.
            If it is run without first running the server, it will fail.

        :seealso:`vimrpyc.vimdini.send_lines_to_houdini`

        '''
        if cls.is_modified():
            cls.send_lines(cls.get_all_lines())
        else:
            cls.send_file(cls.get_current_file_path())

    @classmethod
    def send_file(cls, filename):
        '''Create a temporary file, based on the given file, and send it.

        This function relies on the rpyc module.

        Example:
            In the interpreter to create the server, run this:
            >>> from vimrpyc import hrpyc
            >>> hrpyc.start_server()

            Now write some script, like so:

            cat ~/temp/file.py
            ```
            hou.hipFile.load('TEST')
            ```

            Next, do this
            send_file('~/temp/file.py')

        Args:
            filename (str): The absolute path to some file to send.

        '''
        if filename in _VIM_CURRENT_FILE_NAMES:
            filename = cls.get_current_file_path()

        filename = os.path.expanduser(filename)

        with open(filename, 'r') as file_:
            data = []
            for line in file_.readlines():
                data.append(line.rstrip('\n'))

        cls.send_lines(data)

        # Add the unmodified file, for reference
        label = 'original file: "{filename}"'.format(filename=filename)
        cls.displayer.add_raw_filename(label=label, name=filename)

    @classmethod
    def send_lines(cls, lines):
        '''Create a temporary file, based on the given file, and send it.

        This function relies on the rpyc module.

        Example:
            In the interpreter to create the server, run this:
            >>> from vimrypc import hrpyc
            >>> hrpyc.start_server()

            Next, do this
            send_lines(['print("Hello, World!")'])

            It should print "Hello, World!" in the server.

        Args:
            lines (lines[str]): The lines of commands to run.

        '''
        data = cls.replacer.reformat_source_lines(lines)

        with tempfile.NamedTemporaryFile(prefix='_tmp', suffix='.py', delete=False) as file_:
            file_.write('\n'.join(data))

        filepath = file_.name
        LOGGER.info('Executing file: "{name}".'.format(name=filepath))

        cls.displayer.capture_and_send(functools.partial(cls.import_and_run, filepath))

        # Add the temporary source file, for reference
        label = 'source file: "{filepath}"'.format(filepath=filepath)
        cls.displayer.add_raw_filename(label=label, name=filepath)

    @staticmethod
    def import_and_run(path):
        '''Try to import the given Python file and run it's "main()" function.'''
        module = imp.load_source('module', path)

        try:
            func = module.main
        except AttributeError:
            return

        func()


def _is_shebang_line(line):
    '''bool: Check if the given line is a shebang line (#!/foo/bar python).'''
    return line.lstrip().startswith('#!')


def _is_encoding_line(line):
    '''bool: Check if the given line is an encoding line.'''
    return line.lstrip().startswith('# -*- coding:')


def _get_index_after_header(lines):
    '''int: Get the index that is after the shebang/encoding comment lines.'''
    line_number = 0

    try:
        shebang = lines[0]
    except IndexError:
        return line_number

    if _is_shebang_line(shebang):
        line_number = 1

    try:
        encoding = lines[1]
    except IndexError:
        return line_number

    if _is_encoding_line(encoding):
        line_number = 2

    return line_number


def _get_index_after_future_imports(lines):
    '''int: Get the index after all __future__ imports are done.'''
    recommended_index = 0

    for index, line in enumerate(lines):
        line = line.strip()
        if _FUTURE_IMPORT.match(line):
            recommended_index = index

    return recommended_index


def _find_insert_index(lines):
    '''int: Get the index where rypc modules should start importing from.'''
    recommended_index = _get_index_after_header(lines)
    after_future_index = _get_index_after_future_imports(lines)

    return max(after_future_index, recommended_index)
