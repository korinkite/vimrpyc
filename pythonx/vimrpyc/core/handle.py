#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A collection of handlers which are used to parse.'''

# IMPORT STANDARD LIBRARIES
import traceback
import tempfile


class Handler(object):

    '''A base class that is used to write output data and return quickfix info.

    This class is specialized to work with Vim, because Vim's quickfix window
    requires very specific keys, but this class could be re-purposed easily
    to support other text editors and IDEs.

    '''

    _key = ''

    def __init__(self):
        '''Do nothing.'''
        super(Handler, self).__init__()

    @classmethod
    def _get_file_handle(cls):
        '''Get the object to write text to.

        If the user specified a specific output filepath, use that.
        If nothing is specified, create a temporary file, instead.

        Returns:
            file: The open handler object.

        '''
        # TODO : Abstract out this vim import. This code should be editor-agnostic
        import vim
        path = vim.eval('g:vimrpyc_{key}_file_path'.format(key=cls._key))

        if path:
            return open(path, 'w')

        return tempfile.NamedTemporaryFile(delete=False, suffix='.log')

    @classmethod
    def process(cls, results):
        '''Select a part of the given results to write to disk and return.

        Args:
            results (dict[str]):
                The data to write to disk. Note: The given object must contain
                "cls._key" or this method will raise a KeyError.

        Returns:
            list[dict[str]]: The information to add to Vim's quickfix window.

        '''
        data = results.get(cls._key)

        if not data:
            return dict()

        filename = cls.write(data)
        return cls.report(filename)

    @classmethod
    def write(cls, data):
        '''str: Write the data to disk and return the log file's path.'''
        with cls._get_file_handle() as handle:
            handle.write(data)

        return handle.name

    @classmethod
    def report(cls, filename):
        '''Return the given filename as data that Vim's quickfix window can use.

        Args:
            filename (str): The name of the file to report.

        Returns:
            list[dict[str]]: The information to add to Vim's quickfix window.

        '''
        return [
            {
                'filename': filename,
                'lnum': 1,
                'text': '{name} file: "{filename}"'.format(name=cls._key, filename=filename),
            },
        ]


class StderrHandler(Handler):

    '''A handler that's built to get the captured stderr stream.'''

    _key = 'stderr'


class StdoutHandler(Handler):

    '''A handler that's built to get the captured stdout stream.'''

    _key = 'stdout'


class TracebackHandler(Handler):

    '''A handler that's built parse and return a traceback and exception.'''

    _key = 'traceback'

    @classmethod
    def process(cls, results):
        '''Select a part of the given results to write to disk and return.

        Args:
            results (dict[str]):
                The data to write to disk. Note: The given object must contain
                "cls._key" or this method will raise a KeyError.

        Returns:
            list[dict[str]]: The information to add to Vim's quickfix window.

        '''
        trace, exception = results.get(cls._key)

        if not trace or not exception:
            return dict()

        filename = cls.write_data(trace, exception)
        return cls.report_data(filename, trace, exception)

    @classmethod
    def write_data(cls, data, exception):
        '''Parse the traceback, conform it for Vim, and write it to disk.

        Args:
            data (traceback): The traceback to parse into a file.
            exception (<exceptions.Exception>): Some exception to print.

        Returns:
            str: The name of the log file that was created for the given data.

        '''
        with tempfile.NamedTemporaryFile(delete=False, suffix='.log') as file_:
            text_lines = traceback.format_tb(data)
            text_lines.insert(0, 'Traceback (most recent call last):\n')
            text_lines.append(str(exception))
            # Note: We assume that each line ends with '\n'
            file_.write(''.join(text_lines))

        return file_.name

    @classmethod
    def report_data(cls, filename, trace, exception):
        '''Get a report for the traceback and exception and return it.

        Args:
            filename (str): The path to where the traceback was written.
            trace (traceback): The traceback to parse into Vim quickfix data.
            exception (<exceptions.Exception>): Some exception to print.

        Returns:
            list[dict[str]]: The items to add to Vim's quickfix window.

        '''
        output = cls.make_quickfix_details(cls.unpack_traceback(trace))
        output.append(
            {
                'filename': output[-1]['filename'],
                'lnum': output[-1]['lnum'],
                'text': str(exception),
            }
        )

        output.extend(cls.report(filename))

        return output

    @staticmethod
    def unpack_traceback(trace):
        '''Look through the traceback and get information about its exception.

        Args:
            trace (<traceback> or list[tuple[str, int, str, str]]):
                The traceback to parse or a list of entries in a traceback.
                If a traceback is given, its entries are found and used.
                If entries are given, they're processed, as-is.

        Returns:
            list[dict[str]]:
                'item' (str): The object / function / situation in the stack.
                'line' (int): The line number where the error occurred on.
                'source' (str): The location of where this error occurred.
                'text': The description of the line.

        '''
        # If we get a Exception object, extract it. Otherwise, just assume that
        # the given object was traceback entries, already
        #
        try:
            entries = traceback.extract_tb(trace)
        except AttributeError:
            entries = trace

        output = []

        source_index = 0
        line_number_index = 1
        item_index = 2
        text_index = 3

        for entry in entries:
            output.append({
                'item': entry[item_index],
                'line': entry[line_number_index],
                'source': entry[source_index],
                'text': entry[text_index],
            })

        return output

    @staticmethod
    def make_quickfix_details(details):
        '''Change the given unpacked traceback data into a dict that Vim can use.

        Args:
            details (list[dict[str]]):
                'item' (str): The object / function / situation in the stack.
                'line' (int): The line number where the error occurred on.
                'source' (str): The location of where this error occurred.
                'text': The description of the line.

        Returns:
            list[dict[str]]:
                'text' (str): The description to use.
                'lnum' (int): The line, starting from the number 1.
                'filename' (str): The absolute or relative path to a file.

        '''
        output = []
        for item in details:
            text = item.get('text') or ''
            output.append({
                'text': text.lstrip(),
                'lnum': item.get('line', 0),
                'filename': item.get('source', '')
            })

        return output
