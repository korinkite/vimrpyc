#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A module that lets you run an object and write its output to disk, in Vim.

The stdout and stderr are captured and sent to log files and, if the any exception was raised while the object was being called, capture that traceback
and then add it to Vim's QuickFix window.

'''

# IMPORT STANDARD LIBRARIES
import abc
import sys
import re

# IMPORT THIRD-PARTY LIBRARIES
import vim  # pylint: disable=E0401

# IMPORT LOCAL LIBRARIES
from . import handle


_SOURCE_COMPILE = re.compile('  File "(?P<path>.+)"')
_ITEM_COMPILE = re.compile(r', in\s+(?P<name>.+)$')
_LINE_COMPILE = re.compile(r'line\s+(?P<number>\d+),')


# Reference: https://stackoverflow.com/a/10743550
import contextlib
@contextlib.contextmanager
def capture():
    import sys
    from cStringIO import StringIO
    oldout,olderr = sys.stdout, sys.stderr
    try:
        out=[StringIO(), StringIO()]
        sys.stdout,sys.stderr = out
        yield out
    finally:
        sys.stdout,sys.stderr = oldout, olderr
        out[0] = out[0].getvalue()
        out[1] = out[1].getvalue()


class Displayer(object):

    '''A base-class that will capture rpyc text and send it to a text editor.'''

    handlers = tuple()

    def __init__(self):
        '''Do nothing.'''
        super(Displayer, self).__init__()

    @staticmethod
    def is_always_open_enabled():
        '''bool: Check if the output of ther user's command should be opened.'''
        return True

    @staticmethod
    def is_open_on_error_enabled():
        '''bool: Auto-open the user's command results if an error occurred.'''
        return True

    @staticmethod
    def capture(func, exclude=True):
        '''Grab the stdout/stderr streams of the given function and and exceptions.

        Args:
            func (callable): Any function that takes no arguments.

        Returns:
            dict[str]:
                This creates 3 keys
                'stdout' (str): The path to where the stdout of this function saved.
                'stderr' (str): The path to where the stderr of this function saved.
                'traceback' (exception.BaseException): A raised exception, if any.

        '''
        trace = None
        exception = None
        with capture() as output:
            try:
                func()
            except Exception as exception_:  # pylint: disable=W0703
                _, _, trace = sys.exc_info()
                exception = exception_

                if exclude:
                    trace = trace.tb_next.tb_next


        results = {
            'stdout': '\n'.join(output),
            'traceback': (trace, exception),
        }

        return results

    @staticmethod
    def _result_has_error(results):
        '''bool: If the given results did not report back empty data.'''
        return results.get('traceback', (None, None)) != (None, None)

    @classmethod
    def capture_and_send(cls, func, clear=True):
        '''Run the given function and output its output in our text editor.

        Args:
            func (callable): The function to call.
            clear (:obj:`bool`, optional): If True, clear existing messages first.

        '''
        results = cls.capture(func)
        details = []

        for handler in cls.handlers:
            info = handler.process(results)

            if info:
                details.extend(info)

        if clear:
            cls.clear()

        if details:
            cls.add(details)

            if cls.is_always_open_enabled() or (cls.is_open_on_error_enabled() and cls._result_has_error(results)):
                cls.open()

    @staticmethod
    @abc.abstractmethod
    def add(items):
        '''Add the given items to the text editor's display.'''
        pass

    @staticmethod
    @abc.abstractmethod
    def add_raw_filename(label, name):
        '''Add the given label and name to the text editor's display.'''
        pass

    @staticmethod
    @abc.abstractmethod
    def clear():
        '''Remove all items in the text editor's item display.'''
        pass

    @staticmethod
    @abc.abstractmethod
    def open():
        '''Show the text editor's display.'''
        pass


class VimDisplayer(Displayer):

    '''A worker that parses output from rpyc and adds it to the quickfix window.

    Reference:
        https://vi.stackexchange.com/questions/5110/quickfix-support-for-python-tracebacks

    Attributes:
        handlers (tuple[:class:`vimrpyc.core.handle.Handler`]):
            The handlers that will be used to parse the rpyc results, write those
            results to disk, and ultimately display those contents, to the user.

    '''

    handlers = (handle.StdoutHandler, handle.StderrHandler, handle.TracebackHandler, )

    def __init__(self):
        '''Do nothing.'''
        super(VimDisplayer, self).__init__()

    @staticmethod
    def is_always_open_enabled():
        '''bool: Check if the output of ther user's command should be opened.'''
        try:
            return bool(int(vim.eval('g:vimrpyc_always_open')))
        except vim.error:
            return True

    @staticmethod
    def is_open_on_error_enabled():
        '''bool: Auto-open the user's command results if an error occurred.'''
        try:
            return bool(int(vim.eval('g:vimrpyc_open_on_error')))
        except vim.error:
            return True

    @staticmethod
    def add(items, mode='add'):
        '''Add the given item(s) to the Vim quickfix window.

        This method works because items is a Python dict and, coincidentally,
        when a dict is printed as a string, it is printed exactly the same way
        Vim's setqflist expects args to be formatted. Neat, right?

        Args:
            items (dict[str]):
            mode (:obj:`str`, optional):
                The command that we use to add to the quickfix window, setqflist,
                has two modes: "add", which appends to quickfix, or "replace"
                which clears the quickfix with whatever it's given.
                Options: ("add", "a", "replace", "r").
                Default: "add".

        Raises:
            ValueError: If the given mode did not match one of the mode options.

        '''
        modes = {
            'add': 'a',
            'a': 'a',
            'replace': 'r',
            'r': 'r',
        }

        try:
            mode = modes[mode]
        except KeyError:
            raise ValueError('Mode: "{mode}" was invalid. Options were, "{opt}".'
                             ''.format(mode=mode, opt=sorted(modes.keys())))

        vim.command("call setqflist({items}, '{mode}')".format(items=items, mode=mode))

    @classmethod
    def add_raw_filename(cls, label, name, mode='add'):
        '''Add the given label and name to the text editor's display.

        Args:
            label (str): The text to display for this item.
            name (str): The path to the file. This can be relative or absolute.
            mode (:obj:`str`, optional):
                The command that we use to add to the quickfix window, setqflist,
                has two modes: "add", which appends to quickfix, or "replace"
                which clears the quickfix with whatever it's given.
                Options: ("add", "a", "replace", "r").
                Default: "add".

        '''
        line = [
            {
                'filename': name,
                'lnum': 1,
                'text': label,
            },
        ]

        cls.add(line, mode=mode)

    @staticmethod
    def clear():
        '''Empty out the contents of the current Vim session's Quickfix buffer.'''
        vim.command('cexpr []')

    @staticmethod
    def open():
        '''Make the Vim QuickFix buffer open.'''
        # When we run copen, the QuickFix window steals the cursor focus and
        # puts the cursor inside the QuickFix window. Use `wincmd p` to set the
        # cursor back to its original position
        #
        vim.command('copen')
        vim.command('wincmd p')
