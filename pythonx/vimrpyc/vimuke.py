#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Send a file to Nuke, using rpyc.

For more information on what rpyc is, look at this documentation:

https://media.readthedocs.io/pdf/rpyc/latest/rpyc.pdf
sidefx.com/docs/houdini/hom/rpc.html

'''

# IMPORT STANDARD LIBRARIES
import re

# IMPORT LOCAL LIBRARIES
from .core import base


class NukeRpycReplacer(base.RpycReplacer):

    '''A Nuke-style class that's used to set up our Nuke-rypc tool.

    Attributes:
        connection_template_line (str):
            The line that will be used to replace our Nuke module(s).
        module_imports (tuple[<_sre.SRE_Pattern>]):
            The modules to replace with connection_template_line.
        rpyc_import (<_sre.SRE_Pattern>):
            The pattern that is used to find if rpyc is imported.
        rpyc_import_line (str):
            The line that is used to import rpyc.

    '''

    connection_template_line = 'connection, {name} = nrpyc.import_remote_module()'
    module_imports = (
        re.compile('import\s+nuke\s+as\s+(?P<namespace>\w+)'),
        re.compile('import\s+(?P<namespace>nuke)'),
    )
    rpyc_import = re.compile('from\s+vimrpyc\.servers\s+import\s+nrpyc')
    rpyc_import_line = 'from vimrpyc.servers import nrpyc'


class Vimuke(base.VimCommands):

    '''The class needed to send/receive Nuke python files to rpyc, in Vim.'''

    replacer = NukeRpycReplacer
