if exists('g:vimrpyc_loaded')
    finish
endif

" Commands
"
command! -nargs=0 VimrpycAutoOpenToogle call Vimrpyc#AutoOpenToggle()

command! -nargs=0 VimdiniBufferSend call Vimrpyc#HoudiniBufferSend()
command! -nargs=1 VimdiniFileSend call Vimrpyc#HoudiniFileSend(<f-args>)

command! -nargs=0 VimyaBufferSend call Vimrpyc#MayaBufferSend()
command! -nargs=1 VimyaFileSend call Vimrpyc#MayaFileSend(<f-args>)

command! -nargs=0 VimukeBufferSend call Vimrpyc#NukeBufferSend()
command! -nargs=1 VimukeFileSend call Vimrpyc#NukeFileSend(<f-args>)

" Plugin mappings
nnoremap <silent> <Plug>(vimrpyc-buffer-send-houdini) :VimdiniBufferSend<CR>
nnoremap <silent> <Plug>(vimrpyc-buffer-send-maya) :VimyaBufferSend<CR>
nnoremap <silent> <Plug>(vimrpyc-buffer-send-nuke) :VimukeBufferSend<CR>
nnoremap <silent> <Plug>(vimrpyc-file-send-houdini) :VimdiniFileSend<CR>
nnoremap <silent> <Plug>(vimrpyc-file-send-maya) :VimyaFileSend<CR>
nnoremap <silent> <Plug>(vimrpyc-file-send-nuke) :VimukeFileSend<CR>


" Create default mappings if they are not defined
if !hasmapto('<Plug>(vimrpyc-buffer-send-houdini)')
    nmap <leader>hb <Plug>(vimrpyc-buffer-send-houdini)
endif

if !hasmapto('<Plug>(vimrpyc-file-send-houdini)')
    nmap <leader>hf <Plug>(vimrpyc-file-send-houdini)
endif

if !hasmapto('<Plug>(vimrpyc-buffer-send-maya)')
    nmap <leader>mb <Plug>(vimrpyc-buffer-send-maya)
endif

if !hasmapto('<Plug>(vimrpyc-file-send-maya)')
    nmap <leader>mf <Plug>(vimrpyc-file-send-maya)
endif

if !hasmapto('<Plug>(vimrpyc-buffer-send-nuke)')
    nmap <leader>nb <Plug>(vimrpyc-buffer-send-nuke)
endif

if !hasmapto('<Plug>(vimrpyc-file-send-maya)')
    nmap <leader>nf <Plug>(vimrpyc-file-send-nuke)
endif


" Functions
"
function! Vimrpyc#AutoOpenToggle()
    let g:vimrpyc_auto_open = 1 - get(g:, 'vimrpyc_auto_open', 1)
    if g:vimrpyc_auto_open
        echo "vimrpyc QuickFix auto-open is turned on"
    else
        echo "vimrpyc QuickFix auto-open is turned off"
    endif
endfunction


function! Vimrpyc#HoudiniBufferSend()
    execute 'pythonx from vimrpyc import vimdini'
    execute 'pythonx vimdini.Vimdini.send_buffer()'
    redraw!
    echo "Buffer was sent to Houdini"
endfunction


function! Vimrpyc#HoudiniFileSend(path)
    execute 'pythonx from vimrpyc import vimdini'
    execute 'pythonx vimdini.Vimdini.send_file("' . a:path . '")'
    redraw!
    echo "File was sent to Houdini"
endfunction


function! Vimrpyc#MayaBufferSend()
    execute 'pythonx from vimrpyc import vimya'
    execute 'pythonx vimya.Vimya.send_buffer()'
    redraw!
    echo "Buffer was sent to Maya"
endfunction


function! Vimrpyc#MayaFileSend(path)
    execute 'pythonx from vimrpyc import vimya'
    execute 'pythonx vimya.Vimya.send_file("' . a:path . '")'
    redraw!
    echo "File was sent to Maya"
endfunction


function! Vimrpyc#NukeBufferSend()
    execute 'pythonx from vimrpyc import vimuke'
    execute 'pythonx vimuke.Vimuke.send_buffer()'
    redraw!
    echo "Buffer was sent to Nuke"
endfunction


function! Vimrpyc#NukeFileSend(path)
    execute 'pythonx from vimrpyc import vimuke'
    execute 'pythonx vimuke.Vimuke.send_file("' . a:path . '")'
    redraw!
    echo "File was sent to Nuke"
endfunction


let g:vimrpyc_loaded = '1'
